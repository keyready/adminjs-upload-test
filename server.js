const port = 5001;

const express = require('express');
const cors = require('cors');
const DB = require('./config/db.connect');
const {
    CategoryModel,
    SubcategoryModel,
    BrandModel,
    CartItemModel,
    CertificateModel,
} = require('./models');

const app = express();

app.use(express.json());
// app.use(express.static(path.resolve(__dirname, '../dist/')));
app.use(cors());

const startServer = async () => {
    try {
        await DB.sync();
        // await DB.sync({ alter: true });
        // await DB.sync({ force: true });

        await app.listen(port, () => {
            console.log(`Server started on http://localhost:${port}`);
        });
    } catch (e) {
        console.log(e);
    }
};

app.use(async (req, res, next) => {
    await new Promise((res) => {
        setTimeout(res, 200);
    });
    next();
});

// app.get('/*', (req, res) => {
//     res.status(200).sendFile(path.resolve(__dirname, '../dist/index.html'));
// });

app.get('/api/items', (req, res) => {
    const { scId } = req.query;

    function generateMatrix(rows, columns) {
        const matrix = [];
        for (let i = 1; i <= rows; i += 1) {
            const row = [];
            for (let j = 1; j <= columns; j += 1) {
                row.push(`Описание ${i} описания ${j}`);
            }
            matrix.push(row);
        }
        return matrix;
    }

    const items = new Array(50).fill(0).map((_, index) => ({
        id: index + 1,
        price: 1350,
        image: `../brands/brand${(index + 1).toString().padStart(2, '0')}.png`,
        title: `Товар #${index + 1}`,
        description: `Дизайн снегозадержателя Snow Kit специально разработан для кровельных покрытий с плавными формами. Снегозадержатель Snow Kit гармонично сочетается с металлочерепицей, подчеркивая форму ее волн и плавные изгибы профиля.`,
        descriptionTitle: [
            ...new Array(3).fill(0).map((_, index) => `Заголовок описания ${index + 1}`),
        ],
        descriptionList: generateMatrix(3, 5),
        availableColors: [
            { name: 'Красный', hex: 'ff0000' },
            { name: 'Зеленый', hex: '00ff00' },
            { name: 'Синий', hex: '0000ff' },
            { name: 'Белый', hex: 'ffffff' },
        ],
        certificate: { name: 'Сертификат на использование', link: 'лолаоптв' },
        installation: { name: 'Инструкция по установке', link: 'jhfdsjgn' },
        categoryId: 1,
        subCategoryId: index >= 25 ? 3 : 36,
        brandId: (index + 1) % 10,
    }));

    res.status(200).json(items);
});

app.get('/api/categories', async (req, res) => {
    const categories = await CategoryModel.findAll({ raw: true });
    if (!categories.length) return res.status(404).json({ message: 'Категорий не найдено' });

    return res.status(200).json(categories);
});

app.get('/api/subcategories', async (req, res) => {
    const { categoryId } = req.query;

    if (~~categoryId === -1) {
        const categories = await SubcategoryModel.findAll({ raw: true });
        if (!categories.length) return res.status(404).json({ message: 'Подкатегорий не найдено' });
        return res.status(200).json(categories);
    }
    const categories = await SubcategoryModel.findAll({ raw: true, where: { categoryId } });
    if (!categories.length) return res.status(404).json({ message: 'Подкатегорий не найдено' });

    return res.status(200).json(categories);
});

app.get('/api/brands', async (req, res) => {
    const { page } = req.query;

    const brands = await BrandModel.findAll({
        raw: true,
        limit: 20,
        offset: (page - 1) * 20,
    });
    if (!brands.length) return res.status(404).json({ message: 'Брендов не найдено' });

    return res.status(200).json(brands);
});

app.get('/api/user_cart', async (req, res) => {
    const categories = await CartItemModel.findAll({ raw: true });
    if (!categories.length) return res.status(200).json({ message: 'Корзина пуста' });

    return res.status(200).json(categories);
});

app.post('/delete_from_cart', async (req, res) => {
    const { itemId } = req.body;

    await CartItemModel.destroy({
        where: { id: itemId },
    });

    return res.status(200).json({ message: 'Успешно удалено!' });
});

app.get('/api/certificates', async (req, res) => {
    const certificates = await CertificateModel.findAll({ raw: true });
    if (!certificates.length) return res.status(404).json({ message: 'Категорий не найдено' });

    return res.status(200).json(certificates);
});

app.post('/clear_cart', async (req, res) => {
    await CartItemModel.destroy({
        where: {},
        truncate: true,
    });

    res.status(200).json({ message: 'Корзина успешно очищена' });
});

app.post('/add_to_cart', async (req, res) => {
    const item = req.body;

    await CartItemModel.create(item, { timestamps: false });

    res.status(200).json({ message: 'Товар успешно добавлен в корзину!' });
});

startServer();
// module.exports = app;
