const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'categories',
    {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        tableName: 'categories',
        timestamps: false,
    },
);
