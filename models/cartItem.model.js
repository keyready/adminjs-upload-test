const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'cartItem',
    {
        title: DataTypes.STRING,
        image: DataTypes.STRING,
        price: DataTypes.INTEGER,
        quantity: DataTypes.INTEGER,
        color: DataTypes.STRING,
    },
    {
        tableName: 'cartItem',
        timestamps: false,
    },
);
