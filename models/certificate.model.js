const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'certificates',
    {
        title: DataTypes.STRING,
        file: DataTypes.STRING,
    },
    {
        tableName: 'certificates',
        timestamps: false,
    },
);
