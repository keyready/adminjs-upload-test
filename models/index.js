const UserModel = require('./user.model');
const ItemModel = require('./item.model');
const BrandModel = require('./brand.model');
const SubcategoryModel = require('./subcategory.model');
const CategoryModel = require('./category.model');
const CartItemModel = require('./cartItem.model');
const CertificateModel = require('./certificate.model');
const ImageModel = require('./image.model');

module.exports = {
    UserModel,
    ItemModel,
    CategoryModel,
    SubcategoryModel,
    BrandModel,
    CartItemModel,
    CertificateModel,
    ImageModel,
};
