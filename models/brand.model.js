const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'brands',
    {
        title: DataTypes.STRING,
        image: DataTypes.STRING,
        relatedItems: DataTypes.INTEGER,
    },
    {
        tableName: 'brands',
        timestamps: false,
    },
);
