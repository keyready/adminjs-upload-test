const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'subcategories',
    {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        queryTitle: {
            type: DataTypes.STRING,
        },
        categoryId: {
            type: DataTypes.INTEGER,
        },
    },
    {
        tableName: 'subcategories',
        timestamps: false,
    },
);
