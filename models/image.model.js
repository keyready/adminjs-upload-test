const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'images',
    {
        title: DataTypes.STRING,
        file: DataTypes.STRING,
    },
    {
        tableName: 'images',
        timestamps: false,
    },
);
