const { DataTypes } = require('sequelize');
const DB = require('../config/db.connect');

module.exports = DB.define(
    'items',
    {
        price: {
            type: DataTypes.INTEGER,
        },
        image: {
            type: DataTypes.STRING,
        },
        title: {
            type: DataTypes.STRING,
        },
        description: {
            type: DataTypes.STRING,
        },
        descriptionTitle: {
            type: DataTypes.ARRAY(DataTypes.STRING),
        },
        descriptionList: {
            type: DataTypes.ARRAY(DataTypes.ARRAY(DataTypes.STRING)),
        },
        availableColors: {
            type: DataTypes.ARRAY(DataTypes.JSONB),
        },
        certificate: {
            type: DataTypes.JSONB,
        },
        installation: {
            type: DataTypes.JSONB,
        },
        category: {
            type: DataTypes.STRING,
        },
        subcategory: {
            type: DataTypes.STRING,
        },
    },
    {
        tableName: 'items',
        timestamps: false,
    },
);
