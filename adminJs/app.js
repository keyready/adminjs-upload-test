import AdminJS from 'adminjs';
import AdminJSExpress from '@adminjs/express';
import express from 'express';
import { Database, Resource } from '@adminjs/sequelize';
import {
    UserModel,
    CategoryModel,
    ItemModel,
    SubcategoryModel,
    BrandModel,
    CartItemModel,
    CertificateModel,
    ImageModel,
    // eslint-disable-next-line import/no-relative-packages
} from '../models/index.js';
import { imagesResource } from './resources/item.resource.js';

const PORT = 9001;

AdminJS.registerAdapter({ Database, Resource });

const start = async () => {
    try {
        const app = express();

        const adminOptions = {
            resources: [
                CartItemModel,
                UserModel,
                CategoryModel,
                SubcategoryModel,
                ItemModel,
                BrandModel,
                CertificateModel,
                ImageModel,
                imagesResource,
            ],
        };

        const admin = new AdminJS(adminOptions);

        const adminRouter = AdminJSExpress.buildRouter(admin);
        app.use(admin.options.rootPath, adminRouter);

        app.listen(PORT, () => {
            console.log(`AdminJS started on http://localhost:${PORT}${admin.options.rootPath}`);
        });
    } catch (error) {
        console.log(error);
    }
};

start();
