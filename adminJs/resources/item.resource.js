import uploadFeature from '@adminjs/upload';
import { ComponentLoader } from 'adminjs';

// eslint-disable-next-line import/no-relative-packages
import { ImageModel, ItemModel } from '../../models/index.js';

const localProvider = {
    bucket: './files',
    opts: {
        baseUrl: './files',
    },
};

const componentLoader = new ComponentLoader();

export const imagesResource = {
    resource: ImageModel,
    options: {
        properties: {
            s3Key: {
                type: 'string',
            },
            bucket: {
                type: 'string',
            },
            mime: {
                type: 'string',
            },
            comment: {
                type: 'textarea',
                isSortable: false,
            },
        },
    },
    features: [
        uploadFeature({
            componentLoader,
            provider: { local: localProvider },
            properties: { file: 'file', key: 's3Key', bucket: 'bucket', mimeType: 'mime' },
            validation: { mimeTypes: ['image/png', 'application/pdf', 'audio/mpeg'] },
        }),
    ],
};
